'use strict';
const fs = require('fs');
let permissions = {};

exports.commands = {};

var loadPerms = exports.loadPerms = function()
{
	console.log('Loading permission file.');
	// permissions:
	//{'1231231232' : pLevel}
	try
	{
		permissions = JSON.parse(fs.readFileSync('permissions', 'utf8'));
	} catch(err)
	{
		console.log('Error loading permissions, writing a fresh permissions file.\nError: ' + err);
		permissions = JSON.parse('{}');
		fs.writeFileSync('./permissions', JSON.stringify(permissions));
	}
};

var savePerms = exports.savePerms = function()
{
	console.log('Writing permissions to file.');
	try
	{
		fs.writeFileSync('./permissions', JSON.stringify(permissions));
		console.log('Permissions written successfully!');
	} catch(err)
	{
		console.log('Error writing to file:\nError: ' + err.stack);
	}
};

var testPerms = exports.testPerms = function(pLevel, uid)
{
	if(pLevel == permLevel.LOW || uid == '109880609458880512') return true;
	if(permissions[uid] && permissions[uid] >= pLevel)
	{
		return true;
	} else
	{
		return false;
	}
};

let permLevel = exports.permLevel =
{
	LOW : 0,
	MEDIUM : 1,
	HIGH : 2,
	DEV : 3
};

let commands = exports.commands =
{
	'myid' :
	{
		'desc' : 'Test.',
		'perm' : permLevel.DEV,
		'main' : (msg, args) =>
		{
			msg.channel.send('Ditt id är: ' + msg.author.id);
		}
	},
	'setperm' :
	{
		'usage' : '!setperm **<username> <permission level (1-3)>**',
		'desc' : 'Sets the user\'s permission level.',
		'perm' : permLevel.HIGH,
		'main' : (msg, args) =>
		{
			//console.log(msg.content);
		}
	},
	'removeperm' :
	{
		'desc' : 'Removes a user from the permission list completely, effectively resetting their permissions to the lowest level',
		'perm' : permLevel.HIGH,
		'main' : (msg, args) =>
		{
			//TODO
		}
	}
};